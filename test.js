'use strict'

const assert = require('assert')
const os = require('os')
const childProcess = require('child_process')
const physicalCpuCount = require('.')

const commands = {
  darwin: 'sysctl -n hw.physicalcpu_max',
  linux: 'lscpu -p | egrep -v "^#" | sort -u -t, -k 2,4 | wc -l',
  windows: 'echo %NUMBER_OF_PROCESSORS%'
}

const command = commands[os.platform()]
const output = childProcess.execSync(command, {encoding: 'utf8'})
const expected = parseInt(output.trim(), 10)
const actual = physicalCpuCount

assert.equal(
  actual,
  expected,
  'Should match OS reported number of physical cores ' +
  '(actual: ' + actual + ', ' +
  'expected: ' + expected + ')'
)
